//
//  main.m
//  Strings-Helper
//
//  Created by Ifrim Alexandru on 4/24/15.
//  Copyright (c) 2015 Alexandru Ifrim PFA. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "StringsParser.h"

/**
 * Allowed arguments:<br/>
 *     -s - Set if the output should be sorted, skip to keep the same order from original .strings file<br/>
 *     -t (old/new) - If present, it will automatically use the old or new translations where duplicates are encoutered (old are considered the ones from top)<br/>
 *     -d - If present, it will remove all the keys that show only once in the strings file (presuming that there's no longer a NSLocalizedString called on the specific key)
 *     <path> - Path to .strings files or .lproj folders (containing .strings files)</br>
 *
 * @note that '-*' arguments must before any <path> arguments */
int main(int argc, const char * argv[]) {
    int retVal = 0;
    @autoreleasepool {
        // insert code here...
        if (argc < 2) {
            NSLog(@"Must pass at least 1 localized .strings file or folder as input");
            retVal = -1;
        }
        else {
            StringParserDuplicates duplicates = StringParserDuplicatesChoose;
            BOOL sort = NO;
            BOOL deleteSingles = NO;
            
            int i = 1;
            while (argv[i][0] == '-') {
                if (argv[i][1] == 's') {
                    sort = YES;
                }
                else if (argv[i][1] == 't') {
                    i++; // skip space after -t
                    if (strcmp(argv[i], "old") == 0) {
                        duplicates = StringParserDuplicatesOld;
                    }
                    else if (strcmp(argv[i], "new") == 0) {
                        duplicates = StringParserDuplicatesNew;
                    }
                    else {
                        NSLog(@"Invalid option for -t: %s", argv[i]);
                        return -1;
                    }
                }
                else if (argv[i][1] == 'd') {
                    deleteSingles = YES;
                }
                else {
                    NSLog(@"Invalid parameter %s", argv[i]);
                    return -1;
                }
                i++;
            }
            
            // loop through all paths and parse
            for (; i < argc; i++) {
                NSString *path = [[NSString alloc] initWithUTF8String:argv[i]];
                StringsParser *parser = [[StringsParser alloc] initWithPath:path];
                parser.sortKeys = sort;
                parser.duplicatesMode = duplicates;
                parser.deleteSingles = deleteSingles;
                if (![parser parse]) {
                    NSLog(@"Expected localized .strings file path or folder: `%@`", path);
                }
            }
        }
    }
    return retVal;
}
