//
//  StringsParser.h
//  Strings-Helper
//
//  Created by Ifrim Alexandru on 4/24/15.
//  Copyright (c) 2015 Alexandru Ifrim PFA. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * TODO:
 * - properly list all added, removed and modified strings
 */

typedef NS_ENUM(NSUInteger, StringParserDuplicates) {
    StringParserDuplicatesOld,
    StringParserDuplicatesNew,
    StringParserDuplicatesChoose,
};

/// For command  line usage - 
@interface StringsParser : NSObject {
    BOOL                                    _abortOnFailure;
    NSString                                *_path;
    
    BOOL                                    _sortKeys;
    StringParserDuplicates                  _duplicatesMode;
    
    BOOL                                    _deleteSingles;
}

-(id)initWithPath:(NSString *)path;
-(BOOL)parse;

@property(nonatomic, assign)    BOOL                        sortKeys;
@property(nonatomic, assign)    StringParserDuplicates      duplicatesMode;
/** If set to YES, it will remove all keys that don't show after //END marker (presuming that there's no longer a NSLocalizedString called on the specific key) */
@property(nonatomic, assign)    BOOL                        deleteSingles;
@end
