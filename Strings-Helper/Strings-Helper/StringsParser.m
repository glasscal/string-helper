//
//  StringsParser.m
//  Strings-Helper
//
//  Created by Ifrim Alexandru on 4/24/15.
//  Copyright (c) 2015 Alexandru Ifrim PFA. All rights reserved.
//

#import "StringsParser.h"

static NSString *const lValueKey        = @"value";
static NSString *const lCommentKey      = @"comment";

@implementation StringsParser
@synthesize sortKeys = _sortKeys;
-(id)initWithPath:(NSString *)path {
    self = [super init];
    if (!self) {
        return nil;
    }
    _path = path;
    _abortOnFailure = NO;
    return self;
}
-(BOOL)parse {
    BOOL success;
    BOOL isDir;
    BOOL isFile = [[NSFileManager defaultManager] fileExistsAtPath:_path isDirectory:&isDir];
    if (isDir) {
        NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:_path error:nil];
        int fileCount;
        for (NSString *filePath in files) {
            if ([[filePath pathExtension] isEqualToString:@"strings"]) {
                fileCount += [self parseFileOut:[_path stringByAppendingPathComponent:filePath]];
            }
        }
        success = (fileCount > 0);
    }
    else if (isFile) {
        success = [self parseFileOut:_path];
    }
    else {
        NSLog(@"Nothing at path: `%@`", _path);
        success = NO;
    }
    return success;
}
-(BOOL)parseFileOut:(NSString *)filePath {
    NSString *parsedContent = [self parseFile:filePath];
    if ([parsedContent length] == 0) {
        return NO;
    }
    else {
        NSError *error;
        [parsedContent writeToFile:filePath atomically:YES encoding:NSUTF16StringEncoding error:&error];
        if (error) {
            NSLog(@"Failed to output content: %@", [error localizedDescription]);
            return NO;
        }
        else {
            return YES;
        }
    }
}
-(NSString *)parseFile:(NSString *)filePath {
    NSError *error = nil;
    NSString *fullInput = [NSString stringWithContentsOfFile:filePath encoding:NSUTF16StringEncoding error:&error];
    if (error) {
        NSLog(@"Failed to read from file: `%@`\n%@", filePath, error);
        return nil;
    }
    
//    BOOL newStringsLast = YES; // place new and modified strings last in the file
    NSMutableDictionary *records = [[NSMutableDictionary alloc] initWithCapacity:400];
    NSMutableArray *keys = [[NSMutableArray alloc] initWithCapacity:400]; // we may need to keep the old key order
    NSMutableSet *oldSingleKeys = [[NSMutableSet alloc] initWithCapacity:400];
    
    NSString *commentStart = @"/* ";
    NSString *commentEnd = @" */";
    NSString *keyStart = @"\"";
    NSString *keyEnd = @"\"";           // keys should not contain quotes (even escaped ones)
    NSString *valueStart = @"\"";
    NSString *valueEnd = @"\";";        // issue may occur if containing a '\";' inside (escaped quotes followed by ';')
    NSString *endMark = @"//END";
    BOOL passedEndMark = NO;

    // read content
    NSScanner *scanner = [NSScanner scannerWithString:fullInput];
    NSString *header;
    [scanner scanUpToString:commentStart intoString:&header];
    while (![scanner isAtEnd]) {
        //-
        // scan
        // comment
        NSString *garbage;
        [scanner scanUpToString:commentStart intoString:&garbage];
        if ([garbage containsString:endMark]) {
            NSAssert(!passedEndMark, @"Already passed the end once - the string file is not in the correct format");
            passedEndMark = YES;
        }
        if ([scanner isAtEnd]) {
            // garbage data at the end of file
            break;
        }
        [scanner scanString:commentStart intoString:nil];
        NSString *comment;
        [scanner scanUpToString:commentEnd intoString:&comment];
        comment = [comment stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
//        comment = [comment stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];

        // key
        [scanner scanUpToString:keyStart intoString:nil];
        [scanner scanString:keyStart intoString:nil];
        NSString *key;
        [scanner scanUpToString:keyEnd intoString:&key];
        [scanner scanString:keyEnd intoString:nil];
        
        // value
        [scanner scanUpToString:valueStart intoString:nil];
        [scanner scanString:valueStart intoString:nil];
        NSString *value;
        [scanner scanUpToString:valueEnd intoString:&value];
        while ([value characterAtIndex:[value length]-1] == '\\' && [value characterAtIndex:[value length]-2] != '\\') {
            // still may have issues if string contains with `\\\"` (multiple \ followed by escaped quotes)
            value = [value stringByAppendingString:valueEnd];
            [scanner scanString:valueEnd intoString:nil];
            NSString *append;
            [scanner scanUpToString:valueEnd intoString:&append];
            value = [value stringByAppendingString:append];
        }
        if ([value isEqualToString:key]) {
            // may apply further changes here
            value = comment;
        }
        else {
            NSLog(@"skipping comment `%@` - using value `%@`", comment, value);
        }
        
        //-
        // validate
        NSDictionary *old = [records valueForKey:key];
        if (old) {
            if (passedEndMark) {
                [oldSingleKeys removeObject:key];
            }
            
            if ([old[lValueKey] isEqualToString:value]) {
                // ignore, same string
                continue;
            }
            else {
                BOOL useOld = NO;
                if (_duplicatesMode == StringParserDuplicatesOld || [old[@"comment"] isEqualToString:comment]) {
                    useOld = YES;
                }
                else if (_duplicatesMode == StringParserDuplicatesNew) {
                    useOld = NO;
                }
                else {
                    NSLog(@"Duplicate detected: \"%@\""
                          "\n1:"
                          "\n%@"
                          "\n\n2:"
                          "\n%@"
                          "\nINPUT DESIRED VALUE (1 or 2): "
                          , key, old[lValueKey], value);
                    int input;
                    do {
                        scanf("%d", &input);
                    } while (input != 1 && input != 2);
                    
                    useOld = (input == 1);
                }
                
                
                if (useOld) {
                    // ignore new value
                    value = old[lValueKey];
                }
                else {
                    // set the new value
                    
                }
            }
        }
        else {
            if (!passedEndMark) {
                [oldSingleKeys addObject:key];
            }
            [keys addObject:key];
        }
        records[key] = @{
                         lValueKey: value,
                         lCommentKey: comment
                         };
    }
    
    // remove single keys
    if (_deleteSingles && passedEndMark) {
        for (NSString *key in oldSingleKeys) {
            NSLog(@"Deleting key: \"%@\"", key);
            [records removeObjectForKey:key];
        }
    }
    
    // sort
    if (_sortKeys) {
        [keys sortUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
            return [obj1 compare:obj2];
        }];
    }
    
    //
    NSMutableString *fullOutput = [[NSMutableString alloc] initWithCapacity:[fullInput length]*2];
    if ([header length] > 0) {
        [fullOutput setString:header];
    }
    for (NSString *key in keys) {
        NSDictionary *rec = records[key];
        if (!rec) {
            NSAssert(_deleteSingles, @"");
            continue;
        }
        
        [fullOutput appendString:commentStart];
        [fullOutput appendString:rec[lCommentKey]];
        [fullOutput appendString:commentEnd];
        [fullOutput appendString:@"\n"];
        
        [fullOutput appendString:keyStart];
        [fullOutput appendString:key];
        [fullOutput appendString:keyEnd];
        [fullOutput appendString:@" = "];
        
        [fullOutput appendString:valueStart];
        [fullOutput appendString:rec[lValueKey]];
        [fullOutput appendString:valueEnd];
        [fullOutput appendString:@"\n\n"];
    }
    [fullOutput appendString:endMark];
    [fullOutput appendString:@"\n"];
    
    return fullOutput;
}
@end
